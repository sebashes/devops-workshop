import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.calculate(expression));

        //Will crash
        expression = " 300 - 99 ";
        assertEquals(201, calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

     /*   //Will crash
        expression = "300+99";
        assertEquals(400, calculatorResource.sum(expression));*/
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

  /*      //Will crash
        expression = "20-2";
        assertEquals(200, calculatorResource.subtraction(expression));*/
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999*100";
        assertEquals(99900, calculatorResource.multiplication(expression));

       /*//Will crash
        expression = "20*2";
        assertEquals(1000, calculatorResource.multiplication(expression));*/
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "900/100";
        assertEquals(9, calculatorResource.division(expression));

    /*    //Will crash
        expression = "20/2";
        assertEquals(1000, calculatorResource.division(expression));*/
        System.out.println(calculatorResource);
    }

    @Test
    public void testSumTwice(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "9+1+100";
        assertEquals(110, calculatorResource.sumTwice(expression));

    /*    //Will crash
        expression = "20+2+1";
        assertEquals(1000, calculatorResource.sumTwice(expression));*/
        System.out.println(calculatorResource);
    }

    @Test
    public void testSubractionTwice(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "91-1-100";
        assertEquals(-10, calculatorResource.subtractionTwice(expression));

    /*    //Will crash
        expression = "20-2-1";
        assertEquals(1000, calculatorResource.subtractionTwice(expression));*/
        System.out.println(calculatorResource);
    }

    @Test
    public void testMultiplicationTwice(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "9*1*100";
        assertEquals(900, calculatorResource.multiplicationTwice(expression));

    /*    //Will crash
        expression = "20*2*1";
        assertEquals(1000, calculatorResource.multiplicationTwice(expression));*/
        System.out.println(calculatorResource);
    }

    @Test
    public void testDivisionTwice(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100/5/2";
        assertEquals(10, calculatorResource.divisionTwice(expression));

    /*    //Will crash
        expression = "20/2/1";
        assertEquals(1000, calculatorResource.divisionTwice(expression));*/
        System.out.println(calculatorResource);
    }
}
