package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;

/**
 * Calculator resource exposed at '/calculator' path
 */
@Path("/calculator")
public class CalculatorResource {

    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text or -1 on error
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Serializable calculate(String expression){
        // Removes all whitespaces
        String expressionTrimmed = expression.replaceAll("\\s+","");

        int result = -1;
        String errorMessage = "It seems that you have tried to do something the system can not handle." +
                "Showing '-1' as result.";

        /*
         * .matches use regex expression to decide if a String matches a given pattern.
         * [0-9]+[+][0-9]+ explained:
         * [0-9]+: a number from 0-9 one or more times. For example 1, 12 and 123.
         * [+]: the operator + one time.
         * The total regex expression accepts for example:
         * 12+34,
         * 1+2,
         * 10000+1000
         *
         * Division by 0 and large numbers will cause an error not handled optimally

         */
        //2 numbers
        if(expressionTrimmed.matches("[0-9]+[+][0-9]+")) result = sum(expressionTrimmed);
        else if(expressionTrimmed.matches("[0-9]+[-][0-9]+")) result = subtraction(expressionTrimmed);
        else if(expressionTrimmed.matches("[0-9]+[*][0-9]+")) result = multiplication(expressionTrimmed);
        else if(expressionTrimmed.matches("[0-9]+[/][0-9]+")) result = division(expressionTrimmed);

        //3 numbers. unknown, then unknown
        else if(expressionTrimmed.matches("[0-9]+[+][0-9]+[+][0-9]+")) result = sumTwice(expressionTrimmed);
        else if(expressionTrimmed.matches("[0-9]+[-][0-9]+[-][0-9]+")) result = subtractionTwice(expressionTrimmed);
        else if(expressionTrimmed.matches("[0-9]+[*][0-9]+[*][0-9]+")) result = multiplicationTwice(expressionTrimmed);
        else if(expressionTrimmed.matches("[0-9]+[/][0-9]+[/][0-9]+")) result = divisionTwice(expressionTrimmed);


        else return "It seems you have tried to do something the system can not handle. ";
        return result;
    }

    /**
     * Method used to calculate a sum expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public int sum(String expression){
        String[] split = expression.split("[+]");

        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);


        return number1 + number2;
    }

    /**
     * Method used to calculate a sum expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public int sumTwice(String expression){
        String[] split = expression.split("[+]");


        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);
        int number3 = Integer.parseInt(split[2]);


        return number1 + number2 + number3;
    }

    /**
     * Method used to calculate a sum expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public int subtractionTwice(String expression){
        String[] split = expression.split("[-]");


        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);
        int number3 = Integer.parseInt(split[2]);


        return number1 - number2 - number3;
    }

    /**
     * Method used to calculate a sum expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public int multiplicationTwice(String expression){
        String[] split = expression.split("[*]");


        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);
        int number3 = Integer.parseInt(split[2]);


        return number1 * number2 * number3;
    }

    /**
     * Method used to calculate a sum expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public int divisionTwice(String expression){
        String[] split = expression.split("[/]");

        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);
        int number3 = Integer.parseInt(split[2]);

        return number1 / number2 / number3;
    }

    /**
     * Method used to calculate a subtraction expression.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    public int subtraction(String expression){
        String[] split = expression.split("[-]");

        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);

        return number1 - number2;
    }


    /**
     * Method used to calculate a multiplication expression.
     * Note that using float number will cause a -1 error.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int.
     */
    public int multiplication(String expression){
        String[] split = expression.split("[*]");

        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);


        return number1*number2;
    }

    /**
     * Method used to calculate a division expression.
     * Note that using float number will cause a -1 error.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int.
     */
    public int division(String expression){
        String[] split = expression.split("[/]");

        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);



        return number1/number2;
    }

}
